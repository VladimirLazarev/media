package com.swedbank.media.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class XssProtectorTest {

  XssProtector protector = new XssProtector();

  @Test
  public void excapeXss() throws Exception {
    String result = protector.escape("<script>alert('XSS')</script>");
    assertEquals("&lt;script&gt;alert('XSS')&lt;/script&gt;", result);
  }

  @Test
  public void escapeAmpersand() throws Exception {
    String result = protector.escape("Boris & Doris");
    assertEquals("Boris &amp; Doris", result);
  }
}