package com.swedbank.media.repository;

import com.swedbank.media.service.Conversation;
import com.swedbank.media.service.SignalingException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class ConversationsTest {

  @Test
  public void createConversation() {
    Conversations conversations = new Conversations();
    Conversation conversation = conversations.create("e1e1");
    assertEquals("e1e1", conversation.getUuid());
    assertEquals(conversations.findBy("e1e1").orElse(null), conversation);
  }

  @Test(expected = SignalingException.class)
  public void doNotAllowDuplicates() {
    Conversations conversations = new Conversations();
    conversations.create("e1e1");
    conversations.create("e1e1");
  }

  @Test
  public void removeConversation() {
    Conversations conversations = new Conversations();
    conversations.create("e1e1");

    conversations.remove("e1e1");
    assertFalse(conversations.findBy("e1e1").isPresent());
  }
}