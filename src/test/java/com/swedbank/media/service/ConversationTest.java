package com.swedbank.media.service;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ConversationTest {

  @Test
  public void noParticipantsForJustCreatedConversation() {
    Conversation conversation = new Conversation("aaa-123");
    assertFalse(conversation.hasParticipants());
  }

  @Test
  public void joinConversation() {
    Participant participant_1 = new Participant("1");
    Participant participant_2 = new Participant("2");

    Conversation conversation = new Conversation("aaa-123");
    conversation.join(participant_1);
    assertTrue(conversation.hasParticipants());

    conversation.join(participant_2);
    assertTrue(conversation.has(participant_1));
    assertTrue(conversation.has(participant_2));
  }

  @Test
  public void safeToJoinSameParticipantTwice() {
    Participant participant = new Participant("1");
    Conversation conversation = new Conversation("aaa-123");
    conversation.join(participant);
    conversation.join(participant);
    assertTrue(conversation.hasParticipants());
    assertTrue(conversation.has(participant));
  }

  @Test
  public void leaveConversation() {
    Participant participant_1 = new Participant("1");
    Participant participant_2 = new Participant("2");
    Conversation conversation = new Conversation("aaa-123");
    conversation.join(participant_1);
    conversation.join(participant_2);

    conversation.leave(participant_1);
    assertTrue(conversation.hasParticipants());
    assertTrue(conversation.has(participant_2));
    assertFalse(conversation.has(participant_1));

    conversation.leave(participant_2);
    assertFalse(conversation.hasParticipants());
    assertFalse(conversation.has(participant_2));
  }
}