package com.swedbank.media.service;

import com.swedbank.media.repository.Participants;
import org.junit.Test;

import java.util.Optional;

import static com.swedbank.media.service.Signal.ANSWER;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MediaMessageFactoryTest {

  @Test
  public void decode() throws Exception {
    MediaMessageFactory factory = new MediaMessageFactory();
    factory.participants = mock(Participants.class);

    Participant from = participant("1");
    Participant to = participant("2");
    when(factory.participants.findBy("1")).thenReturn(Optional.of(from));
    when(factory.participants.findBy("2")).thenReturn(Optional.of(to));

    MediaMessage message = factory.decode("{\"from\":\"1\",\"to\":\"2\",\"signal\":\"answer\",\"conversationUuid\":\"111-EEE\",\"content\":\"free text\"}");
    assertSame(from, message.getFrom());
    assertSame(to, message.getTo());
    assertSame(ANSWER, message.getSignal());
    assertEquals("111-EEE", message.getConversationUuid());
    assertEquals("free text", message.getContent());
  }

  @Test
  public void decodeNullValues() throws Exception {
    MediaMessageFactory factory = new MediaMessageFactory();

    MediaMessage message = factory.decode("{\"from\": null,\"to\":null,\"signal\":null,\"conversationUuid\":null,\"content\":null}");
    assertNull(message.getFrom());
    assertNull(message.getTo());
    assertNull(message.getContent());
    assertNull(message.getConversationUuid());
    assertEquals(Signal.EMPTY, message.getSignal());
  }

  @Test
  public void encode() throws Exception {
    MediaMessageFactory factory = new MediaMessageFactory();
    MediaMessage mediaMessage = new MediaMessage(participant("1"), participant("2"), ANSWER, "111-EEE", "free text", true);
    assertEquals("{\"from\":\"1\",\"to\":\"2\",\"signal\":\"answer\",\"conversationUuid\":\"111-EEE\",\"content\":\"free text\",\"renegotiation\":\"true\"}", factory.encode(mediaMessage));
  }

  private Participant participant(String id) {
    Participant participant = mock(Participant.class);
    when(participant.getId()).thenReturn(id);
    return participant;
  }
}