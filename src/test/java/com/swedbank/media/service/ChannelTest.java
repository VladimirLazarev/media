package com.swedbank.media.service;

import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.Broadcaster;
import org.junit.Test;

import java.util.Arrays;

import static org.mockito.Mockito.*;

public class ChannelTest {

  @Test
  public void broadcastMessageToAllResources() throws Exception {
    MediaMessage message = mock(MediaMessage.class);
    AtmosphereResource resource = mock(AtmosphereResource.class);
    Broadcaster broadcaster_1 = mock(Broadcaster.class);
    Broadcaster broadcaster_2 = mock(Broadcaster.class);
    when(resource.broadcasters()).thenReturn(Arrays.asList(broadcaster_1, broadcaster_2));

    Channel channel = new Channel(resource);
    channel.broadcast(message);

    verify(broadcaster_1).broadcast(message, resource);
    verify(broadcaster_2).broadcast(message, resource);
  }
}