import com.swedbank.media.initializers.ConfigProperties;
import com.swedbank.media.initializers.SpringMvcInitializer;
import com.swedbank.media.initializers.SpringSecurityInitializer;
import org.eclipse.jetty.annotations.AnnotationConfiguration;
import org.eclipse.jetty.annotations.ClassInheritanceHandler;
import org.eclipse.jetty.plus.webapp.EnvConfiguration;
import org.eclipse.jetty.plus.webapp.PlusConfiguration;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.util.ConcurrentHashSet;
import org.eclipse.jetty.webapp.*;
import org.eclipse.jetty.websocket.jsr356.server.deploy.WebSocketServerContainerInitializer;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

public class MediaServerLauncher {

  public static void main(String[] args) throws Exception {
    ConfigProperties.getInstance();
    Server server = new Server();

    int HTTP_PORT = 7200;
    WebAppContext webAppContext = new WebAppContext("/", "/*");
    webAppContext.setContextPath("/signalling");
    webAppContext.setResourceBase("webapp");
    webAppContext.setClassLoader(Thread.currentThread().getContextClassLoader());
    webAppContext.getSessionHandler().setMaxInactiveInterval(60 * 15);
    webAppContext.setAttribute("org.eclipse.jetty.websocket.jsr356", "true");
    webAppContext.setConfigurations(new Configuration[]{
      new WebInfConfiguration(),
      new FragmentConfiguration(),
      new EnvConfiguration(),
      new PlusConfiguration(),
      new MetaInfConfiguration(),
      new AnnotationConfiguration() {
        @Override
        public void preConfigure(WebAppContext context) throws Exception {
          ClassInheritanceMap classInheritanceMap = new ClassInheritanceMap();
          ConcurrentHashSet<String> set = new ConcurrentHashSet<>();
          set.add(SpringMvcInitializer.class.getName());
          set.add(SpringSecurityInitializer.class.getName());
          classInheritanceMap.put(WebApplicationInitializer.class.getName(), set);
          context.setAttribute(CLASS_INHERITANCE_MAP, classInheritanceMap);
          _classInheritanceHandler = new ClassInheritanceHandler(classInheritanceMap);
        }
      }
    });

    webAppContext.setInitParameter("contextClass", AnnotationConfigWebApplicationContext.class.getName());
    webAppContext.setParentLoaderPriority(true);

    HttpConfiguration config = new HttpConfiguration();
    config.setSecureScheme("https");
    config.setSendServerVersion(true);
    config.setSecurePort(7211);
    config.setOutputBufferSize(32786);
    config.setRequestHeaderSize(8192);
    config.setResponseHeaderSize(8192);
    config.addCustomizer(new ForwardedRequestCustomizer());
    config.addCustomizer(new SecureRequestCustomizer());

    HttpConnectionFactory httpConnectionFactory = new HttpConnectionFactory(config);
    ServerConnector httpConnector = new ServerConnector(server, httpConnectionFactory);
    httpConnector.setPort(HTTP_PORT);

    ServerConnector connector = new ServerConnector(server, httpConnectionFactory);
    connector.setPort(7211);

    server.setConnectors(new Connector[] { httpConnector, connector});
    server.setHandler(webAppContext);

    WebSocketServerContainerInitializer.configureContext(webAppContext);

    server.start();
    server.join();
  }
}

