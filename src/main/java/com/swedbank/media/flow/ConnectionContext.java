package com.swedbank.media.flow;

import com.swedbank.media.service.MediaMessage;
import com.swedbank.media.service.Participant;
import com.swedbank.media.service.Signal;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class ConnectionContext {

  public enum State {
    NOT_INITIALIZED {
      @Override
      public boolean isValid(MediaMessage message) {
        return false;
      }
    },
    OFFER_REQUESTED {
      @Override
      public boolean isValid(MediaMessage message) {
        return Signal.OFFER_RESPONSE.is(message.getSignal());
      }
    },
    ANSWER_REQUESTED {
      @Override
      public boolean isValid(MediaMessage message) {
        return Signal.ANSWER_RESPONSE.is(message.getSignal());
      }
    },
    EXCHANGE_CANDIDATES {
      @Override
      public boolean isValid(MediaMessage message) {
        return Signal.CANDIDATE.is(message.getSignal());
      }
    };

    public abstract boolean isValid(MediaMessage message);
  }

  @Value("${webrtc.max_connection_setup_time:30}")
  private int maxConnectionSetupTime;

  private State state = State.NOT_INITIALIZED;
  private DateTime lastUpdated = DateTime.now();
  private boolean renegotiation;

  private String conversationUuid;
  private Participant from;
  private Participant to;

  public ConnectionContext(Participant from, Participant to, String conversationUuid) {
    this.from = from;
    this.to = to;
    this.conversationUuid = conversationUuid;
  }

  public Participant getFrom() {
    return from;
  }

  public Participant getTo() {
    return to;
  }

  public String getConversationUuid() {
    return conversationUuid;
  }

  public boolean isValidForState(MediaMessage message, State state) {
    return state.equals(this.state) && state.isValid(message);
  }

  public void renegotiate() {
    renegotiation = true;
  }

  public void stopRenegotiation() {
    renegotiation = false;
  }

  public boolean isRenegotiation() {
    return renegotiation;
  }

  public void updateState(State state) {
    this.state = state;
    lastUpdated = DateTime.now();
  }

  public boolean isActual() {
    return lastUpdated.plusSeconds(maxConnectionSetupTime).isAfter(DateTime.now());
  }
}
