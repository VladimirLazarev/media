package com.swedbank.media.flow;

import com.swedbank.media.repository.Connections;
import com.swedbank.media.repository.Conversations;
import com.swedbank.media.repository.UuidRegistry;
import com.swedbank.media.service.Conversation;
import com.swedbank.media.service.MediaMessage;
import com.swedbank.media.service.MessageSender;
import com.swedbank.media.service.Participant;
import com.swedbank.media.util.XssProtector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.swedbank.media.flow.ConnectionContext.State.*;
import static com.swedbank.media.service.SignalingException.Exceptions.CONVERSATION_NOT_FOUND;
import static com.swedbank.media.service.SignalingException.Exceptions.INVALID_RECIPIENT;

@Component
public class ConversationFlow {

  @Autowired
  private UuidRegistry uuidRegistry;

  @Autowired
  private Conversations conversations;

  @Autowired
  private Connections connections;

  @Autowired
  private MessageSender messageSender;

  @Autowired
  private ApplicationContext context;

  @Autowired
  private XssProtector xssProtector;

  public void process(MediaMessage message) {
    if (message.isCreate() || message.isJoin()) {
      join(message.getFrom(), message.getConversationUuid());
      return;
    }
    if (message.isLeft() && conversations.hasConversationFor(message.getFrom())) {
      left(message.getFrom(), message.getConversationUuid());
      return;
    }
    if (message.isNegotiate()) {
      renegotiate(message.getFrom(), message.getConversationUuid());
      return;
    }

    Optional<Conversation> conversation = conversations.findBy(message.getConversationUuid());

    if (message.isChatMessage() && conversation.isPresent()) {
      String text = xssProtector.escape(message.getContent());
      conversation.get().forEachParticipant(p -> sendChatMessage(message, text, p));
      return;
    }

    ConnectionContext connectionContext = connections.get(message.getFrom(), message.getTo());

    if (connectionContext.isValidForState(message, OFFER_REQUESTED)) {
      answerRequest(connectionContext, message);
      connectionContext.updateState(ANSWER_REQUESTED);
    }
    else if (connectionContext.isValidForState(message, ANSWER_REQUESTED)) {
      approve(connectionContext, message);
      connectionContext.stopRenegotiation();
      connectionContext.updateState(EXCHANGE_CANDIDATES);
    }
    else if (connectionContext.isValidForState(message, EXCHANGE_CANDIDATES)) {
      exchangeCandidates(message);
      connectionContext.updateState(EXCHANGE_CANDIDATES);
    }
  }

  private void begin(Participant from, Participant to, String conversationUuid) {
    ConnectionContext connectionContext = context.getBean(ConnectionContext.class, from, to, conversationUuid);
    connections.put(from, to, connectionContext);
    sendOfferRequest(connectionContext);
    connectionContext.updateState(OFFER_REQUESTED);
  }

  private void join(Participant joiner, String conversationUuid) {
    uuidRegistry.checkAllowed(conversationUuid);
    Optional<Conversation> optional = conversations.findBy(conversationUuid);
    Conversation conversation = optional.isPresent() ? optional.get() : conversations.create(conversationUuid);
    join(conversation, joiner);
  }

  private void join(Conversation conversation, Participant participant) {
    if (conversation.hasParticipants()) {
      messageSender.sendJoinMessage(participant, participant, conversation.getUuid());
    }
    else {
      messageSender.confirmConversationCreated(participant, conversation.getUuid());
    }

    conversation.forEachParticipant(p -> {
      sendJoinMessage(conversation, participant, p);
      begin(p, participant, conversation.getUuid());
    });

    conversation.join(participant);
  }

  public void left(Participant leaving, String conversationUuid) {
    Conversation conversation = conversations.findBy(conversationUuid).orElseThrow(CONVERSATION_NOT_FOUND::exception);
    if (!conversation.has(leaving)) {
      throw INVALID_RECIPIENT.exception();
    }

    conversation.leave(leaving);
    conversation.forEachParticipant(participant -> sendLeftMessage(conversation, leaving, participant));

    if (!conversation.hasParticipants()) {
      conversations.remove(conversation.getUuid());
      messageSender.notifyConversationClosed(leaving, conversation.getUuid());
    }
  }

  private void renegotiate(Participant from, String conversationUuid) {
    Optional<Conversation> conversation = conversations.findBy(conversationUuid);
    conversation.ifPresent(c -> c.forEachParticipant(p -> {
      if (!p.equals(from)) {
        ConnectionContext connectionContext = connections.get(from, p);
        connectionContext.renegotiate();
        sendOfferRequest(connectionContext);
        connectionContext.updateState(OFFER_REQUESTED);
      }
    }));
  }

  private void sendLeftMessage(Conversation conversation, Participant leaving, Participant to) {
    messageSender.sendLeftMessage(leaving, to, conversation.getUuid());
  }

  private void sendJoinMessage(Conversation conversation, Participant from, Participant to) {
    messageSender.sendJoinMessage(from, to, conversation.getUuid());
  }

  private void sendOfferRequest(ConnectionContext connectionContext) {
    messageSender.sendOfferRequest(connectionContext.getTo(), connectionContext.getFrom(), connectionContext.getConversationUuid(), connectionContext.isRenegotiation());
  }

  private void sendChatMessage(MediaMessage message, String text, Participant to) {
    messageSender.sendChatMessage(message.getFrom(), to, message.getConversationUuid(), text);
  }

  private void exchangeCandidates(MediaMessage message) {
    messageSender.sendExchangeCandidates(message.getFrom(), message.getTo(), message.getConversationUuid(), message.getContent());
  }

  private void approve(ConnectionContext connectionContext, MediaMessage message) {
    messageSender.sendApprovalMessage(connectionContext.getTo(), connectionContext.getFrom(), message.getConversationUuid(), message.getContent());
  }

  private void answerRequest(ConnectionContext connectionContext, MediaMessage message) {
    messageSender.sendAnswerRequest(connectionContext.getFrom(), connectionContext.getTo(), message.getConversationUuid(), message.getContent(), connectionContext.isRenegotiation());
  }
}
