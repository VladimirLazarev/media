package com.swedbank.media.util;

import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
public class XssProtector {

  private static final Pattern andPattern = Pattern.compile("&", Pattern.MULTILINE);
  private static final Pattern ltPattern = Pattern.compile("<", Pattern.MULTILINE);
  private static final Pattern gtPattern = Pattern.compile(">", Pattern.MULTILINE);

  public String escape(String value) {
    value = andPattern.matcher(value).replaceAll("&amp;");
    value = ltPattern.matcher(value).replaceAll("&lt;");
    value = gtPattern.matcher(value).replaceAll("&gt;");
    return value;
  }
}
