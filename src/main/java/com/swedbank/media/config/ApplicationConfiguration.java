package com.swedbank.media.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.concurrent.ScheduledExecutorFactoryBean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceView;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

import static java.lang.Integer.valueOf;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.swedbank.media"})
@PropertySource(value = {"classpath:config.properties"}, name = ApplicationConfiguration.APPLICATION_CONFIG_FILE)
public class ApplicationConfiguration extends WebMvcConfigurerAdapter {

  public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX";
  public static final String APPLICATION_CONFIG_FILE = "applicationConfigurationFileName";

  @Autowired
  Environment environment;

  @Value("${json.prettyPrint:false}")
  private boolean jsonPrettyPrint;

  @Value("${webrtc.scheduler_size:10}")
  private int size;

  @Bean
  public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
    return new PropertySourcesPlaceholderConfigurer();
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    Integer cacheExpirationSeconds = valueOf(environment.getProperty("cacheExpirationSeconds", "2"));
    registry.addResourceHandler("js/**").addResourceLocations("js/").setCachePeriod(cacheExpirationSeconds).resourceChain(false);
    registry.addResourceHandler("css/**").addResourceLocations("css/").setCachePeriod(cacheExpirationSeconds).resourceChain(false);
    registry.addResourceHandler("img/**").addResourceLocations("img/").setCachePeriod(cacheExpirationSeconds).resourceChain(false);
    registry.addResourceHandler("views/**").addResourceLocations("views/").setCachePeriod(cacheExpirationSeconds).resourceChain(false);
  }

  @Bean
  public InternalResourceViewResolver viewResolver() {
    InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
    viewResolver.setViewClass(InternalResourceView.class);
    viewResolver.setPrefix("/views/");
    viewResolver.setSuffix(".html");
    viewResolver.setCache(!"dev".equals(environment.getProperty("environment")));
    return viewResolver;
  }

  @Override
  public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
    converters.add(mappingJackson2HttpMessageConverter());
    converters.add(byteArrayHttpMessageConverter());
    super.configureMessageConverters(converters);
  }

  @Bean
  public RestTemplate serviceLayerTemplate() {
    return new RestTemplate();
  }

  @Bean
  public Jackson2ObjectMapperFactoryBean objectMapperFactory() {
    Jackson2ObjectMapperFactoryBean factoryBean = new Jackson2ObjectMapperFactoryBean();
    factoryBean.setSimpleDateFormat("dd/MM/yyyy");
    return factoryBean;
  }

  @Bean
  public MultipartResolver multipartResolver() {
    return new StandardServletMultipartResolver();
  }

  @Bean
  public ByteArrayHttpMessageConverter byteArrayHttpMessageConverter() {
    return new ByteArrayHttpMessageConverter();
  }

  @Bean
  public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
    MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
    converter.setObjectMapper(objectMapperFactory().getObject());
    return converter;
  }

  @Bean
  public GsonHttpMessageConverter gsonHttpMessageConverter() {
    GsonHttpMessageConverter converter = new GsonHttpMessageConverter();
    converter.setGson(gson());
    return converter;
  }

  @Bean
  public Gson gson() {
    GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat(DATE_FORMAT).setVersion(2.0);
    if (jsonPrettyPrint) {
      gsonBuilder.setPrettyPrinting();
    }
    return gsonBuilder.create();
  }

  @Bean(name = "mediaServerScheduler")
  public ScheduledExecutorService scheduler() {
    ScheduledExecutorFactoryBean factoryBean = new ScheduledExecutorFactoryBean();
    factoryBean.setThreadNamePrefix("mediaServerScheduler");
    factoryBean.setPoolSize(size);
    factoryBean.afterPropertiesSet();
    return factoryBean.getObject();
  }
}
