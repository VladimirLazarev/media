package com.swedbank.media.config;

import com.swedbank.media.endpoints.SocketIOBroadcaster;
import org.atmosphere.client.TrackMessageSizeInterceptor;
import org.atmosphere.cpr.*;
import org.atmosphere.socketio.cpr.SocketIOAtmosphereInterceptor;
import org.atmosphere.spring.bean.AtmosphereSpringContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.ServletException;
import java.util.HashMap;
import java.util.Map;

import static org.atmosphere.cpr.BroadcasterLifeCyclePolicy.ATMOSPHERE_RESOURCE_POLICY.IDLE_DESTROY;

@Configuration
public class AtmosphereConfig {

  @Bean
  public AtmosphereFramework atmosphereFramework() throws ServletException, InstantiationException, IllegalAccessException {
    AtmosphereFramework atmosphereFramework = new AtmosphereFramework(false, true);
    atmosphereFramework.interceptor(new TrackMessageSizeInterceptor());
    atmosphereFramework.interceptor(new SocketIOAtmosphereInterceptor());
    return atmosphereFramework;
  }

  @Bean
  public BroadcasterFactory broadcasterFactory() throws ServletException, InstantiationException, IllegalAccessException {
    return atmosphereFramework().getAtmosphereConfig().getBroadcasterFactory();
  }

  @Bean
  public AtmosphereSpringContext atmosphereSpringContext() {
    AtmosphereSpringContext atmosphereSpringContext = new AtmosphereSpringContext();
    Map<String, String> map = new HashMap<>();
    map.put("org.atmosphere.cpr.dropAccessControlAllowOriginHeader", "false");
    map.put("org.atmosphere.cpr.broadcasterClass", SocketIOBroadcaster.class.getName());
    map.put("org.atmosphere.cpr.broadcasterLifeCyclePolicy", IDLE_DESTROY.toString());
    map.put(AtmosphereInterceptor.class.getName(), TrackMessageSizeInterceptor.class.getName());
    map.put(AnnotationProcessor.class.getName(), DefaultAnnotationProcessor.class.getName());
    atmosphereSpringContext.setConfig(map);
    return atmosphereSpringContext;
  }
}