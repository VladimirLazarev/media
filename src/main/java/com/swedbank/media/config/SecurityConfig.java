package com.swedbank.media.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import java.util.Map;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  public static final String CONVERSATION_REGISTRATOR_ROLE = "REGISTRATOR";

  @Autowired
  Environment environment;

  @SuppressWarnings("unchecked")
  @Autowired
  public void registerGlobalAuthentication(AuthenticationManagerBuilder auth) throws Exception {
    InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> builder = auth.inMemoryAuthentication();
    getCredentials().entrySet().forEach(e -> builder.withUser(e.getKey()).password(e.getValue()).roles(CONVERSATION_REGISTRATOR_ROLE));
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests().antMatchers("/conversation/**").hasRole(CONVERSATION_REGISTRATOR_ROLE);
    http.httpBasic().realmName("MediaServer Realm");
    http.headers().frameOptions().sameOrigin().httpStrictTransportSecurity().disable();
    http.csrf().ignoringAntMatchers("/binding/**").disable();
  }

  @SuppressWarnings("unchecked")
  private Map<String, String> getCredentials() {
    return new SpelExpressionParser().parseExpression(environment.getRequiredProperty("auth.basic.credentials")).getValue(Map.class);
  }
}