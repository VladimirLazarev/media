package com.swedbank.media.repository;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import static java.util.Arrays.asList;

@Repository
public class UuidRegistry {

  private static final String ALLOWED_UUID_PROPERTY = "allowed";

  private Set<String> allowedConversationUuids = new HashSet<>();
  private Properties storageProperties = new Properties();
  private File storageFile;

  @Autowired
  Environment environment;

  @Value("${enableConversationWhiteList:false}")
  Boolean enableConversationWhiteList;

  @Value("${conversationsRegistryFile:#{null}}")
  String conversationsRegistryFile;

  @PostConstruct
  private void init() {
    if (enableConversationWhiteList) {
      resolveRegistryFile();
      readStorage();
    }
  }

  public synchronized void register(String conversationUuid) {
    allowedConversationUuids.add(conversationUuid);
    writeStorage();
  }

  public synchronized void unregister(String conversationUuid) {
    allowedConversationUuids.remove(conversationUuid);
    writeStorage();
  }

  public synchronized void checkAllowed(String conversationUuid) {
    if (enableConversationWhiteList && !allowedConversationUuids.contains(conversationUuid)) {
      throw new IllegalStateException("The conversation UUID " + conversationUuid + " is unknown");
    }
  }

  private void resolveRegistryFile() {
    this.storageFile = new File(conversationsRegistryFile);
    if (!this.storageFile.exists()) {
      throw new IllegalStateException("Cannot find registry file " + conversationsRegistryFile);
    }
  }

  private void readStorage() {
    try (InputStream is = new FileInputStream(storageFile)) {
      storageProperties.load(is);
      String allowedIds = storageProperties.getProperty(ALLOWED_UUID_PROPERTY);
      if (allowedIds != null && !allowedIds.isEmpty()) {
        allowedConversationUuids.addAll(asList(allowedIds.split(",")));
      }
    }
    catch (Exception e) {
      throw new IllegalStateException("Unable to read registry file");
    }
  }

  private void writeStorage() {
    try (FileOutputStream os = new FileOutputStream(storageFile)) {
      storageProperties.setProperty(ALLOWED_UUID_PROPERTY, StringUtils.join(allowedConversationUuids, ","));
      storageProperties.store(os, "");
    }
    catch (Exception e) {
      throw new IllegalStateException("Unable to write registry file");
    }
  }
}
