package com.swedbank.media.repository;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.swedbank.media.flow.ConnectionContext;
import com.swedbank.media.service.Participant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Repository
public class Connections {

  private Table<Participant, Participant, ConnectionContext> connections = HashBasedTable.create();

  @Autowired
  @Qualifier("mediaServerScheduler")
  private ScheduledExecutorService scheduler;

  @Value("${webrtc.max_connection_setup_time:30}")
  private int maxConnectionSetupTime;

  @PostConstruct
  void releaseObsoleteConnections() {
    scheduler.scheduleWithFixedDelay(() -> {
      List<ConnectionContext> oldConnections = connections.values().stream()
        .filter(context -> !context.isActual())
        .collect(Collectors.toList());
      oldConnections.forEach(c -> connections.remove(c.getFrom(), c.getTo()));

    }, maxConnectionSetupTime, maxConnectionSetupTime, TimeUnit.SECONDS);
  }

  public void put(Participant from, Participant to, ConnectionContext ctx) {
    connections.put(from, to, ctx);
    connections.put(to, from, ctx);
  }

  public ConnectionContext get(Participant from, Participant to) {
    ConnectionContext connectionContext = connections.get(from, to);
    if (connectionContext == null) {
      throw new IllegalStateException("Unable to find connection for " + from + " -> " + to);
    }
    return connectionContext;
  }
}
