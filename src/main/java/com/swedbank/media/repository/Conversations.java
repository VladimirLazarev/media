package com.swedbank.media.repository;

import com.swedbank.media.service.Conversation;
import com.swedbank.media.service.Participant;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static com.swedbank.media.service.SignalingException.Exceptions.CONVERSATION_UUID_OCCUPIED;
import static com.swedbank.media.service.SignalingException.Exceptions.INVALID_CONVERSATION_UUID;
import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Repository
public class Conversations {

	private Map<String, Conversation> conversations = new ConcurrentHashMap<>();

	public Optional<Conversation> findBy(String conversationId) {
		return isEmpty(conversationId) ? empty() : ofNullable(conversations.get(conversationId));
	}

	public Optional<Conversation> findBy(Participant participant) {
		return conversations.values().stream().filter(el -> el.has(participant)).findFirst();
	}

	public boolean hasConversationFor(Participant participant) {
		return conversations.values().stream().anyMatch(el -> el.has(participant));
	}

	public Conversation create(String conversationUuid) {
		validate(conversationUuid);
		Conversation conversation = new Conversation(conversationUuid);
		conversations.put(conversation.getUuid(), conversation);
		return conversation;
	}

	public void remove(String conversationUuid) {
		conversations.remove(conversationUuid);
	}

	private void validate(String conversationUuid) {
		if (isEmpty(conversationUuid)) {
			throw INVALID_CONVERSATION_UUID.exception();
		}
		if (findBy(conversationUuid).isPresent()) {
			throw CONVERSATION_UUID_OCCUPIED.exception();
		}
	}
}
