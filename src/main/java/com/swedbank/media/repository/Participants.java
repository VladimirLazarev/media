package com.swedbank.media.repository;

import com.swedbank.media.service.Channel;
import com.swedbank.media.service.MessageSender;
import com.swedbank.media.service.Participant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

@Repository
public class Participants {

	@Autowired
	@Qualifier("mediaServerScheduler")
	private ScheduledExecutorService scheduler;

	@Autowired
	MessageSender messageSender;

	@Value("${webrtc.ping_period:3}")
	private int period;

	private Map<String, Participant> participants = new ConcurrentHashMap<>();
	private Map<Participant, ScheduledFuture<?>> pings = new ConcurrentHashMap<>();

	public Optional<Participant> findBy(Channel channel) {
		return findBy(channel.getId());
	}

	public Optional<Participant> findBy(String id) {
		return id == null ? empty() : ofNullable(participants.get(id));
	}

	public Participant register(Channel channel) {
    Participant participant = participants.get(channel.getId());
    if (participant == null) {
			participant = new Participant(channel);
			participants.put(channel.getId(), participant);
			pings.put(participant, schedulePing(participant));
		}
		return participant;
	}

	public void unregister(Channel channel) {
		findBy(channel).ifPresent(participant -> {
      ScheduledFuture<?> scheduledFuture = pings.get(participant);
      scheduledFuture.cancel(true);
      pings.remove(participant);
    });
		participants.remove(channel.getId());
	}

	private ScheduledFuture<?> schedulePing(Participant participant) {
		return scheduler.scheduleAtFixedRate(() -> messageSender.sendPingMessage(participant), period, period, TimeUnit.SECONDS);
	}
}
