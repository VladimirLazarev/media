package com.swedbank.media.service;

import com.google.gson.*;
import com.swedbank.media.repository.Participants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class MediaMessageFactory {

  @Autowired
  Participants participants;

  private final Gson gson = createJsonBuilder().create();

  public MediaMessage decode(String json) {
    return gson.fromJson(json, MediaMessage.class);
  }

  public String encode(MediaMessage mediaMessage) {
    return gson.toJson(mediaMessage);
  }

  private GsonBuilder createJsonBuilder() {
    return new GsonBuilder().serializeNulls().disableInnerClassSerialization()
      .registerTypeAdapter(MediaMessage.class, (JsonSerializer<MediaMessage>) (src, typeOfSrc, context) -> {
        JsonObject obj = new JsonObject();
        obj.addProperty("from", src.getFrom() != null ? src.getFrom().getId() : "SYSTEM");
        obj.addProperty("to", src.getTo() != null ? src.getTo().getId() : "SYSTEM");
        obj.addProperty("signal", src.getSignal().ordinaryName());
        obj.addProperty("conversationUuid", src.getConversationUuid());
        obj.addProperty("content", src.getContent());
        if (src.isRenegotiation()) {
          obj.addProperty("renegotiation", "true");
        }
        return obj;
      })
      .registerTypeAdapter(MediaMessage.class, (JsonDeserializer<MediaMessage>) (jsonElement, typeOfSrc, context) -> {
        if (jsonElement.isJsonPrimitive()) {
          throw new IllegalArgumentException("Illegal content json: " + jsonElement.getAsString());
        }

        JsonObject jsonObj = (JsonObject)jsonElement;
        String from = getProperty(jsonObj, "from");
        String to = getProperty(jsonObj, "to");
        String signalStr = getProperty(jsonObj, "signal");
        String conversationUuid = getProperty(jsonObj, "conversationUuid");
        String content = getProperty(jsonObj, "content");

        Participant sender = from != null ? participants.findBy(from).orElse(new Participant(from)) : null;
        Participant recipient = to != null ? participants.findBy(to).orElse(new Participant(to)) : null;
        Signal signal = Arrays.stream(Signal.values()).filter(el -> el.is(signalStr)).findFirst().orElse(Signal.EMPTY);

        return new MediaMessage(sender, recipient, signal, conversationUuid, content, false);
      });
  }

  private String getProperty(JsonObject jsonObj, String name) {
    JsonElement jsonElement = jsonObj.get(name);
    return jsonElement != null && !jsonElement.isJsonNull() ? jsonElement.getAsString() : null;
  }
}

