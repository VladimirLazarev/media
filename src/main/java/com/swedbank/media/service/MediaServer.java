package com.swedbank.media.service;

import com.swedbank.media.flow.ConversationFlow;
import com.swedbank.media.repository.Conversations;
import com.swedbank.media.repository.Participants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.swedbank.media.service.SignalingException.Exceptions.PARTICIPANT_NOT_FOUND;

@Component
public class MediaServer {

	@Autowired
	private Participants participants;

	@Autowired
	private Conversations conversations;

	@Autowired
	private ConversationFlow conversationFlow;

	public void register(Channel channel) {
		participants.register(channel);
	}

	public void unregister(Channel channel) {
		unbind(channel);
	}

	public void handle(MediaMessage mediaMessage, Channel channel) {
		Participant sender = findParticipant(channel);
		mediaMessage.setFrom(sender);
		conversationFlow.process(mediaMessage);
	}

  public void handleError(Channel channel, String reason) {
    unbind(channel);
  }

	private Participant findParticipant(Channel channel) {
		return participants.findBy(channel).orElseThrow(() -> new SignalingException(PARTICIPANT_NOT_FOUND));
	}

	private void unbind(Channel channel) {
		participants.findBy(channel).ifPresent(p -> conversations.findBy(p).ifPresent(c -> conversationFlow.left(p, c.getUuid())));
		participants.unregister(channel);
	}
}
