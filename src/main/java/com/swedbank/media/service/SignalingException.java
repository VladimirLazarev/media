package com.swedbank.media.service;

import org.apache.commons.lang3.StringUtils;

import static java.lang.String.format;

public class SignalingException extends RuntimeException {

	private static final long serialVersionUID = 4171073365651049929L;

	public enum Exceptions {
		PARTICIPANT_NOT_FOUND("0001"),
		INVALID_RECIPIENT("0002"),
		INVALID_CONVERSATION_UUID("0101"),
		CONVERSATION_UUID_OCCUPIED("0102"),
		CONVERSATION_NOT_FOUND("0103");

		private String code;

		Exceptions(String code) {
			this.code = code;
		}

		public String getErrorCode() {
			return code;
		}

		public SignalingException exception() {
			return new SignalingException(this);
		}

		public SignalingException exception(String customMesage) {
			return new SignalingException(this, customMesage);
		}

		public SignalingException exception(Exception reason) {
			return new SignalingException(this, reason);
		}
	}

	private String errorCode;
	private String customMessage;

	public SignalingException(Exceptions exception) {
		super(exception.getErrorCode() + ": " + exception.name());
		this.errorCode = exception.getErrorCode();
	}

	public SignalingException(Exceptions exception, Throwable t) {
		super(exception.getErrorCode() + ": " + exception.name(), t);
		this.errorCode = exception.getErrorCode();
	}

	public SignalingException(Exceptions exception, String customMessage) {
		super(exception.getErrorCode() + ": " + exception.name());
		this.errorCode = exception.getErrorCode();
		this.customMessage = customMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getCustomMessage(){
		return StringUtils.defaultString(customMessage);
	}

	@Override
	public String toString() {
		return format("Signaling Exception (CODE: %s) %s [%s]", getErrorCode(), getMessage(), getCustomMessage());
	}

}
