package com.swedbank.media.service;

import org.atmosphere.cpr.AtmosphereResource;

public class Channel {

  private AtmosphereResource resource;

  public Channel(AtmosphereResource resource) {
    this.resource = resource;
  }

  public String getId() {
    return resource.uuid();
  }

  public void broadcast(MediaMessage message) {
    resource.broadcasters().forEach(b -> {
      try {
        b.broadcast(message, resource);
      }
      catch (Exception ex) {
        throw new RuntimeException("Unable to send message: " + message);
      }
    });
  }
}
