package com.swedbank.media.service;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Participant {

	private String id;
	private Channel channel;

	public Participant(Channel channel) {
		this(channel.getId());
		this.channel = channel;
	}

	public Participant(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public Channel getChannel() {
		return channel;
	}

	@Override
	public String toString() {
		return String.format("%s", getId());
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Participant)) {
			return false;
		}
		Participant m = (Participant) o;
		return new EqualsBuilder().append(m.getId(), getId()).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(getId()).build();
	}
}
