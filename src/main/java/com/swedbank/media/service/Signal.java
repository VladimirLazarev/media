package com.swedbank.media.service;

public enum Signal {
	EMPTY {
		@Override
		public String ordinaryName() {
			return "";
		}
	},
	OFFER {
		@Override
		public String ordinaryName() {
			return "offer";
		}
	},
	OFFER_RESPONSE {
		@Override
		public String ordinaryName() {
			return "offerResponse";
		}
	},
	ANSWER {
		@Override
		public String ordinaryName() {
			return "answer";
		}
	},
	ANSWER_RESPONSE {
		@Override
		public String ordinaryName() {
			return "answerResponse";

		}
	},
	APPROVE {
		@Override
		public String ordinaryName() {
			return "approve";
		}
	},
	CANDIDATE {
		@Override
		public String ordinaryName() {
			return "candidate";
		}
	},
	PING {
		@Override
		public String ordinaryName() {
			return "ping";
		}
	},
	LEFT {
		@Override
		public String ordinaryName() {
			return "left";
		}
	},
	JOIN {
		@Override
		public String ordinaryName() {
			return "join";
		}
	},
	JOINED {
		@Override
		public String ordinaryName() {
			return "joined";
		}
	},
	CREATED {
		@Override
		public String ordinaryName() {
			return "created";
		}
	},
	CLOSED {
		@Override
		public String ordinaryName() {
			return "closed";
		}
	},
	NEGOTIATE {
		@Override
		public String ordinaryName() {
			return "negotiate";
		}
	},
	CHAT_MESSAGE {
		@Override
		public String ordinaryName() {
			return "chatMessage";
		}
	},;

	public boolean is(String string) {
        return ordinaryName().equalsIgnoreCase(string);
	}

	public boolean is(Signal signal) {
		return this.equals(signal);
	}

	public abstract String ordinaryName();

}
