package com.swedbank.media.service;

import java.io.Serializable;

public class MediaMessage implements Serializable {

  private Participant from;
  private Participant to;
  private Signal signal;
  private String conversationUuid;
  private String content;
  private boolean renegotiation;

  MediaMessage(Participant from, Participant to, Signal signal, String conversationUuid, String content, boolean renegotiation) {
    this.from = from;
    this.to = to;
    this.signal = signal;
    this.conversationUuid = conversationUuid;
    this.content = content;
    this.renegotiation = renegotiation;
  }

  public void setFrom(Participant from) {
    this.from = from;
  }

  public Participant getFrom() {
    return from;
  }

  public Participant getTo() {
    return to;
  }

  public Signal getSignal() {
    return signal;
  }

  public String getConversationUuid() {
    return conversationUuid;
  }

  public String getContent() {
    return content;
  }

  public boolean isRenegotiation() {
    return renegotiation;
  }

  public boolean isCreate() {
    return Signal.CREATED.is(signal);
  }

  public boolean isJoin() {
    return Signal.JOIN.is(signal);
  }

  public boolean isLeft() {
    return Signal.LEFT.is(signal);
  }

  public boolean isChatMessage() {
    return Signal.CHAT_MESSAGE.is(signal);
  }

  public boolean isCandidate() {
    return Signal.CANDIDATE.is(signal);
  }

  public boolean isNegotiate() {
    return Signal.NEGOTIATE.is(signal);
  }

  @Override
  public String toString() {
    return String.format("[%s]: (%s -> %s) : %s", signal, from, to, conversationUuid);
  }

  public static MessageBuilder create() {
    return new MessageBuilder();
  }

  public static class MessageBuilder {
    private Participant from;
    private Participant to;
    private Signal signal;
    private String conversationUuid;
    private String content;
    private boolean renegotiation;

    public MediaMessage build() {
      return new MediaMessage(from, to, signal, conversationUuid, content, renegotiation);
    }

    public MessageBuilder from(Participant from) {
      this.from = from;
      return this;
    }

    public MessageBuilder to(Participant to) {
      this.to = to;
      return this;
    }

    public MessageBuilder signal(Signal signal) {
      this.signal = signal;
      return this;
    }

    public MessageBuilder conversationUuid(String conversationUuid) {
      this.conversationUuid = conversationUuid;
      return this;
    }

    public MessageBuilder content(String content) {
      this.content = content;
      return this;
    }

    public MessageBuilder renegotiation(boolean renegotiation) {
      this.renegotiation = renegotiation;
      return this;
    }
  }
}
