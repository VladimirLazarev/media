package com.swedbank.media.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static com.swedbank.media.service.MediaMessage.create;
import static com.swedbank.media.service.Signal.*;

@Component
public class MessageSender {

  private static final Logger LOG = LoggerFactory.getLogger(MessageSender.class);

  public void confirmConversationCreated(Participant creator, String conversationUuid) {
    send(create()
      .to(creator)
      .signal(CREATED)
      .conversationUuid(conversationUuid)
      .build());
  }

  public void sendJoinMessage(Participant joined, Participant recipient, String conversationUuid) {
    send(create()
      .from(joined)
      .to(recipient)
      .signal(JOINED)
      .conversationUuid(conversationUuid)
      .build());
  }

  public void sendLeftMessage(Participant leaving, Participant recipient, String conversationUuid) {
    send(create()
      .from(leaving)
      .to(recipient)
      .conversationUuid(conversationUuid)
      .signal(LEFT)
      .build());
  }

  public void sendOfferRequest(Participant sender, Participant recipient, String conversationUuid, boolean renegotiation) {
    send(create()
      .from(sender)
      .to(recipient)
      .signal(OFFER)
      .conversationUuid(conversationUuid)
      .renegotiation(renegotiation)
      .build());
  }

  public void sendAnswerRequest(Participant sender, Participant recipient, String conversationUuid, String content, boolean renegotiation) {
    send(create()
      .from(sender)
      .to(recipient)
      .signal(ANSWER)
      .conversationUuid(conversationUuid)
      .content(content)
      .renegotiation(renegotiation)
      .build());
  }

  public void sendApprovalMessage(Participant from, Participant to, String conversationUuid, String content) {
    send(create()
      .from(from)
      .to(to)
      .signal(APPROVE)
      .conversationUuid(conversationUuid)
      .content(content)
      .build());
  }

  public void sendExchangeCandidates(Participant from, Participant to, String conversationUuid, String content) {
    send(create()
      .from(from)
      .to(to)
      .signal(CANDIDATE)
      .conversationUuid(conversationUuid)
      .content(content)
      .build());
  }

  public void sendChatMessage(Participant sender, Participant receiver, String conversationUuid, String message) {
    send(create()
      .from(sender)
      .to(receiver)
      .signal(CHAT_MESSAGE)
      .conversationUuid(conversationUuid)
      .content(message)
      .build());
  }

  public void notifyConversationClosed(Participant participant, String conversationUuid) {
    send(create()
      .to(participant)
      .signal(CLOSED)
      .conversationUuid(conversationUuid)
      .build());
  }

  public void sendPingMessage(Participant receiver) {
    send(create()
      .to(receiver)
      .signal(PING)
      .build());
  }

  private void send(MediaMessage message) {
    if (message.getSignal() != PING) {
      LOG.info(message.toString());
    }
    message.getTo().getChannel().broadcast(message);
  }
}
