package com.swedbank.media.service;

import com.google.common.collect.Sets;

import java.util.Set;
import java.util.function.Consumer;

public class Conversation {

	private String uuid;
	private Set<Participant> participants = Sets.newConcurrentHashSet();

	public Conversation(String uuid) {
		this.uuid = uuid;
	}

	public String getUuid() {
		return uuid;
	}

	public boolean hasParticipants() {
		return !participants.isEmpty();
	}

	public boolean has(Participant participant) {
		return participant != null && participants.contains(participant);
	}

	public void forEachParticipant(Consumer<Participant> consumer) {
    participants.forEach(consumer);
  }

  public void join(Participant participant) {
    participants.add(participant);
  }

	public void leave(Participant participant) {
		participants.remove(participant);
	}

	@Override
	public String toString() {
		return "Conversation{uuid='" + uuid + "\'}";
	}
}
