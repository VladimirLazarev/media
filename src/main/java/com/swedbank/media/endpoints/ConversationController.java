package com.swedbank.media.endpoints;

import com.swedbank.media.repository.UuidRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@RequestMapping("/conversation")
public class ConversationController {

  protected static final String UTF8_JSON = "application/json;charset=utf-8";

  @Autowired
  UuidRegistry uuidRegistry;

  @ResponseBody
  @RequestMapping(value = "/register/{conversationUuid}", method = GET, produces = UTF8_JSON)
  public String register(@PathVariable("conversationUuid") String conversationUuid) {
    uuidRegistry.register(conversationUuid);
    return "registered";
  }

  @ResponseBody
  @RequestMapping(value = "/unregister/{conversationUuid}", method = GET, produces = UTF8_JSON)
  public String unregister(@PathVariable("conversationUuid") String conversationUuid) {
    uuidRegistry.unregister(conversationUuid);
    return "unregistered";
  }
}