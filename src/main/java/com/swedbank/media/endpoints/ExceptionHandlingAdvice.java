package com.swedbank.media.endpoints;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ExceptionHandlingAdvice {

  static Logger LOG = LoggerFactory.getLogger(ExceptionHandlingAdvice.class);

  @Autowired
  protected Gson gson;

  @ResponseBody
  @ExceptionHandler(value = {Exception.class})
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public Map<String,String> generalError(Exception exception) throws Exception {
    LOG.error("Unexpected error", exception);
    Map<String,String> params = new HashMap<>();
    params.put("generalError", exception.getMessage());
    return params;
  }
}
