package com.swedbank.media.endpoints;

import com.swedbank.media.service.Channel;
import com.swedbank.media.service.MediaMessageFactory;
import com.swedbank.media.service.MediaServer;
import org.atmosphere.config.service.AtmosphereHandlerService;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.BroadcasterFactory;
import org.atmosphere.socketio.SocketIOSessionOutbound;
import org.atmosphere.socketio.cache.SocketIOBroadcasterCache;
import org.atmosphere.socketio.cpr.SocketIOAtmosphereHandler;
import org.atmosphere.socketio.transport.DisconnectReason;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

@AtmosphereHandlerService(
  path = "/binding/*",
  broadcasterCache=SocketIOBroadcasterCache.class,
  supportSession=true)
public class ChannelsBindingEndpoint extends SocketIOAtmosphereHandler {

  private static final Logger LOG = LoggerFactory.getLogger(ChannelsBindingEndpoint.class);

  @Autowired
  BroadcasterFactory broadcasterFactory;

  @Autowired
  MediaMessageFactory messageFactory;

  @Autowired
  MediaServer mediaServer;

  @Override
  public void onConnect(AtmosphereResource resource, SocketIOSessionOutbound outbound) throws IOException {
    LOG.info("Browser {" + resource.uuid() + "} connected.");
    mediaServer.register(new Channel(resource));
  }

  @Override
  public void onMessage(AtmosphereResource resource, SocketIOSessionOutbound outbound, String message) {
    try {
      mediaServer.handle(messageFactory.decode(message), new Channel(resource));
    }
    catch (Exception ex) {
      LOG.error("Message processing failed", ex);
    }
  }

  @Override
  public void onDisconnect(AtmosphereResource resource, SocketIOSessionOutbound outbound, DisconnectReason reason) {
    Channel channel = new Channel(resource);
    if (resource.isCancelled()) {
      LOG.info("Browser {" + resource.uuid() + "} unexpectedly disconnected. Reason: " + reason);
      mediaServer.handleError(channel, translateReason(reason));
    }
    else if (DisconnectReason.CLOSED_REMOTELY.equals(reason)) {
      LOG.info("Browser {" + resource.uuid() + "} closed the connection");
      mediaServer.unregister(channel);
    }
  }

  private String translateReason(DisconnectReason reason) {
    return String.valueOf(reason.value());
  }
}