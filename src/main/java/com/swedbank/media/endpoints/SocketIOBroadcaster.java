package com.swedbank.media.endpoints;

import com.swedbank.media.service.MediaMessage;
import com.swedbank.media.service.MediaMessageFactory;
import org.atmosphere.config.service.BroadcasterService;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.DefaultBroadcaster;
import org.atmosphere.socketio.transport.SocketIOPacketImpl;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;
import java.util.concurrent.Future;

import static org.atmosphere.socketio.transport.SocketIOPacketImpl.PacketType.MESSAGE;

@BroadcasterService
public class SocketIOBroadcaster extends DefaultBroadcaster {

  @Autowired
  MediaMessageFactory messageFactory;

  @Override
  public Future<Object> broadcast(Object message) {
    Object packet = prepareSocketPacket((MediaMessage) message);
    return super.broadcast(packet);
  }

  @Override
  public Future<Object> broadcast(Object message, AtmosphereResource resource) {
    Object packet = prepareSocketPacket((MediaMessage) message);
    return super.broadcast(packet, resource);
  }

  @Override
  public Future<Object> broadcast(Object message, Set<AtmosphereResource> subset) {
    Object packet = prepareSocketPacket((MediaMessage) message);
    return super.broadcast(packet, subset);
  }

  private Object prepareSocketPacket(MediaMessage message) {
    return new SocketIOPacketImpl(MESSAGE, messageFactory.encode(message)).toString();
  }
}
