package com.swedbank.media.initializers;

import com.swedbank.media.config.ApplicationConfiguration;
import org.atmosphere.spring.bean.AtmosphereSpringServlet;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class SpringMvcInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

  public SpringMvcInitializer() {
    ConfigProperties.getInstance(); // init properties
  }

  @Override
  public void onStartup(ServletContext servletContext) throws ServletException {
    System.setProperty("net.sf.ehcache.skipUpdateCheck","true");
    super.onStartup(servletContext);
  }

  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class<?>[] {ApplicationConfiguration.class};
  }

  @Override
  protected Class<?>[] getServletConfigClasses() {
    return new Class[0];
  }

  @Override
  protected String[] getServletMappings() {
    return new String[] { "/" };
  }

  @Override
  protected void registerDispatcherServlet(ServletContext servletContext) {
    super.registerDispatcherServlet(servletContext);
    initAtmosphereServlet(servletContext);
  }

  private void initAtmosphereServlet(ServletContext servletContext) {
    ServletRegistration.Dynamic registration = servletContext.addServlet("AtmosphereFramework", new AtmosphereSpringServlet());
    registration.setInitParameter("org.atmosphere.cpr.packages", "com.swedbank.media.endpoints");
    registration.setInitParameter("socketio-transport", "websocket,xhr-polling,jsonp-polling");
    registration.setInitParameter("socketio-timeout", "25000");
    registration.setInitParameter("socketio-heartbeat", "15000");
    registration.setInitParameter("socketio-suspendTime", "30000");

    registration.setInitParameter("socketio-timeout", "25000");
    registration.setInitParameter("org.atmosphere.cpr.broadcaster.shareableThreadPool", "true");
    registration.setInitParameter("org.atmosphere.cpr.broadcaster.maxProcessingThreads", "20");
    registration.setInitParameter("org.atmosphere.cpr.broadcaster.maxAsyncWriteThreads", "20");
    registration.setInitParameter("org.atmosphere.interceptor.HeartbeatInterceptor.clientHeartbeatFrequencyInSeconds", "10");
    servletContext.addListener(new org.atmosphere.cpr.SessionSupport());

    registration.addMapping("/binding/*");
    registration.setLoadOnStartup(0);
    registration.setAsyncSupported(true);
  }
}
