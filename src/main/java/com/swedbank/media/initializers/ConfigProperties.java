package com.swedbank.media.initializers;

import org.apache.log4j.MDC;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

public class ConfigProperties extends Properties {

  private static final String INSTANCE_NAME = "InstanceName";
  private static final String SECRET_PROPERTIES = "config.properties";

  private static final String instanceName = readInstanceName();
  private static final ConfigProperties instance = loadConfigProperties();

  public static ConfigProperties getInstance() {
    return instance;
  }

  public static String getInstanceName() {
    return instanceName;
  }

  private static ConfigProperties loadConfigProperties() {
    ConfigProperties properties = new ConfigProperties();
    try (InputStream inputStream = ConfigProperties.class.getClassLoader().getResourceAsStream(SECRET_PROPERTIES)) {
      if (inputStream != null) {
        properties.load(inputStream);
      }
      else {
        throw new FileNotFoundException("property file '" + SECRET_PROPERTIES + "' not found in the classpath");
      }

      initLogger(properties);
    }
    catch (IOException ex) {
      throw new RuntimeException("Cannot load application properties", ex);
    }

    return properties;
  }

  private static void initLogger(ConfigProperties properties) {
    MDC.put(INSTANCE_NAME, getInstanceName());
    System.setProperty("environment", properties.getProperty("environment"));
  }

  private static String readInstanceName() {
    String instanceName = null;
    try {
      instanceName = InetAddress.getLocalHost().getHostName();
    }
    catch (UnknownHostException ignore) {
    }

    return instanceName;
  }
}
