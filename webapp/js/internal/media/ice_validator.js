function ICECandidateValidator(config) {

  var pc;

  this.pingTurn = function(peerConfig, successCallback, failCallback) {
    var candidates = [];
    try {
      pc = new RTCPeerConnection(peerConfig);
      pc.onicecandidate = function (event) {
        if (event.candidate) {
          var c = parseCandidate(event.candidate.candidate);
          candidates.push(c);
        }
        else {
          pc.close();
          pc = null;

          if (candidates.length > 0) {
            successCallback(candidates);
          }
          else {
            failCallback("no ice candidates");
          }
        }
      };
      pc.createOffer({offerToReceiveAudio: 1}).then(
        function(desc) {
          pc.setLocalDescription(desc);
        },
        function(error) {
          failCallback(error);
        })
    }
    catch (ex) {
      failCallback(ex);
    }
  };

// Parse a candidate:foo string into an object, for easier use by other methods.
  function parseCandidate(text) {
    var candidateStr = 'candidate:';
    var pos = text.indexOf(candidateStr) + candidateStr.length;
    var fields = text.substr(pos).split(' ');
    return {
      'component': fields[1],
      'type': fields[7],
      'foundation': fields[0],
      'protocol': fields[2],
      'address': fields[4],
      'port': fields[5],
      'priority': fields[3]
    };
  }
}
