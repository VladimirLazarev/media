function MeetingCtrl($rootScope, $scope, $timeout, $window, $controller) {
  $controller('mainCtrl', {$rootScope: $rootScope, $scope: $scope });

  var media_options = {
    audio: true,
    video: true,
    //bitsPerSecond: 256 * 8 * 1024,
    mimeType: 'video/webm'
  };

  var localVideo = $('#local')[0];
  var remoteVideo = $('#remote')[0];
  var localRecorderStateElement = $('#localRecorderState');
  var remoteRecorderStateElement = $('#remoteRecorderState');
  var canvas = $('#snapshotCanvas')[0];
  var localMediaRecorder;
  var remoteMediaRecorder;
  var recordingStatePoller;

  $scope.models = {
    configuration: {
      applicationId: '123',
      conversationUuid: '12345-12345',
      mediaServerHost: 'localhost',
      mediaServerPort: 7200
    },

    showJoinConversation: true,
    showLeaveConversation: false,
    showSnapshot : false,
    showLocalVideo: false,
    showRemoteVideo: false,
    showSnapshots: false,
    showChat: false,
    disabledSnapshot: false,
    meetingEnded: false,
    replayVideo: false,
    comments: "",
    chatMessage: "",
    technicalInfo: {
      localRecorder: "inactive",
      remoteRecorder: "inactive",
      mediaServerState: "undefined",
      turnServerState: "undefined",
      webrtcError: null
    }
  };

  $scope.methods = {
    startRecording: function () {
      // todo: invoke recorder. The media server does not provide recording functionality
    },

    stopRecording: function() {
      // todo: invoke recorder. The media server does not provide recording functionality
    },

    joinConversation: function () {
      webRTC.join();
      $scope.models.showLocalVideo = true;
      $scope.models.showJoinConversation = false;
      $scope.models.showLeaveConversation = true;
      $scope.models.showChat = true;
    },

    leaveConversation: function () {
      webRTC.leave();
      $scope.models.showSnapshot = false;
      $scope.models.showLeaveConversation = false;
      localVideo.srcObject = null;
      $scope.models.showLocalVideo = false;

      $scope.methods.closeRemoteVideo();
      $scope.methods.stopRecording();

      $scope.models.showJoinConversation = true;
    },

    sendChatMessage: function () {
      if ($scope.models.chatMessage != "") {
        webRTC.sendChatMessage($scope.models.chatMessage);
        $scope.models.chatMessage = "";
      }
    },

    sendCapturedDataToServer: function (streamName, streamData) {
    },

    appendSnapshot: function(dataUrl) {
      var imageElement = document.createElement("img");
      imageElement.setAttribute("src", dataUrl);
      imageElement.setAttribute("style", "padding:5px; width: 20%;");

      var imageLink = document.createElement("a");
      imageLink.setAttribute("href", dataUrl);
      imageLink.setAttribute("data-gallery", "");

      $(imageLink).append(imageElement);
      $('#snapshots').append(imageLink);
      $scope.models.showSnapshots = true;
    },

    takeSnapshot: function () {
      $scope.models.disabledSnapshot = true;

      var canvasContext = canvas.getContext('2d');
      canvasContext.drawImage(remoteVideo, 0, 0);
      var dataUrl = canvas.toDataURL('image/png');
      var myBase64Data = dataUrl.split(',')[1];

      // onboardingService.saveSnapshot(myBase64Data, function () {
      //   $timeout(function() {
      //     $scope.methods.appendSnapshot(dataUrl);
      //     $scope.models.disabledSnapshot = false;
      //   }, 0);
      // });
    },

    initStreamRecorder: function(streamName, streamToRecord) {
      try {
        return new RecordHandler(streamName, streamToRecord, media_options, $scope.methods.sendCapturedDataToServer);
      }
      catch (e) {
        console.error('Exception while creating MediaRecorder: ' + e);
      }
    },

    openRemoteVideo: function(stream) {
      $timeout(function() {
        remoteVideo.srcObject = stream.stream;
        $scope.models.showRemoteVideo = true;
      }, 0);
    },

    closeRemoteVideo: function() {
      remoteVideo.srcObject = null;
      $scope.models.showRemoteVideo = false;
    },

    finalizeConversation: function() {
      $scope.models.showChat = false;
      $scope.models.showFinalizeConversation = false;
      $scope.models.showJoinConversation = false;
    }
  };

  var webRTC = null;
  WebRTC.onReady = function () {

    var peer_config = {
      iceServers: [{
          url: 'stun:stun.l.google.com:19302'
        }]
    };

    new ICECandidateValidator().pingTurn(peer_config,
      function (candidates) {
        $timeout(function() {
          $scope.models.technicalInfo.turnServerState = "connected";
        }, 0);
      },
      function (error) {
        $timeout(function() {
          $scope.models.technicalInfo.turnServerState = "failed to connect. Error: " + error;
        }, 0);
      }
    );

    webRTC = new WebRTC({
      conversationUuid: $scope.models.configuration.conversationUuid,
      host: $scope.models.configuration.mediaServerHost,
      port: $scope.models.configuration.mediaServerPort,
      mediaConfig: {
        video: true,
        audio: true
      },
      peerConfig: peer_config
    });
    webRTC.on('connected', function (webrtc, event) {
      $timeout(function() {
        $scope.models.technicalInfo.mediaServerState = "connected";
      }, 0);
    });
    webRTC.on('closed', function (webrtc, event) {
      webRTC.disconnect();
      $timeout(function() {
        $scope.models.technicalInfo.mediaServerState = "disconnected";
      }, 0);
    });
    webRTC.on('created', function (webrtc, event) {
      console.info('CREATED: ' + JSON.stringify(event));
    });
    webRTC.on('joined', function (webrtc, event) {
      console.info('JOINED: ' + JSON.stringify(event));
    });
    webRTC.on('left', function (webrtc, event) {
      console.info("LEFT: " + JSON.stringify(event));
      $scope.methods.stopRecording();
      $scope.methods.closeRemoteVideo();
      webRTC.closePeerConnections();
    });
    webRTC.on('localStream', function (webrtc, stream) {
      localVideo.srcObject = stream.stream;
    });
    webRTC.on('remoteStream', function (webrtc, stream) {
      localMediaRecorder = $scope.methods.initStreamRecorder("local", localVideo.srcObject);
      remoteMediaRecorder = $scope.methods.initStreamRecorder("remote", stream.stream);
      $scope.methods.openRemoteVideo(stream);
      $scope.models.showSnapshot = true;
      $scope.methods.startRecording();
    });
    webRTC.on('error', function (webrtc, error) {
      $timeout(function() {
        $scope.models.technicalInfo.mediaServerState = "connection failed";
        console.error(error);
      }, 0);
    });
  };
}