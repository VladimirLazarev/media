'use strict';

function RecordHandler(streamName, mediaStream, config, streamSender) {
  if (!mediaStream) {
    throw 'MediaStream is mandatory.';
  }

  var mediaRecorder;
  var self = this;

  function startRecording() {
    if (mediaRecorder) {
      stopRecording();
    }

    mediaRecorder = new MediaStreamRecorder(streamName, mediaStream, config, streamSender);
    mediaRecorder.record();
    return self;
  }

  function stopRecording() {
    if (!mediaRecorder) {
      return console.warn('It seems that "startRecording" is not invoked for ' + config.type + ' recorder');
    }

    mediaRecorder.stop();
  }

  function getState() {
    return mediaRecorder ? mediaRecorder.getState() : "inactive";
  }

  return {
    getState: getState,
    startRecording: startRecording,
    stopRecording: stopRecording
  };
}

var AudioContext = window.AudioContext;

if (typeof AudioContext === 'undefined') {
  if (typeof webkitAudioContext !== 'undefined') {
    /*global AudioContext:true */
    AudioContext = webkitAudioContext;
  }

  if (typeof mozAudioContext !== 'undefined') {
    /*global AudioContext:true */
    AudioContext = mozAudioContext;
  }
}

/*jshint -W079 */
var URL = window.URL;

if (typeof URL === 'undefined' && typeof webkitURL !== 'undefined') {
  /*global URL:true */
  URL = webkitURL;
}

if (typeof navigator !== 'undefined') { // maybe window.navigator?
  if (typeof navigator.webkitGetUserMedia !== 'undefined') {
    navigator.getUserMedia = navigator.webkitGetUserMedia;
  }

  if (typeof navigator.mozGetUserMedia !== 'undefined') {
    navigator.getUserMedia = navigator.mozGetUserMedia;
  }
}

var isEdge = navigator.userAgent.indexOf('Edge') !== -1 && (!!navigator.msSaveBlob || !!navigator.msSaveOrOpenBlob);
var isOpera = !!window.opera || navigator.userAgent.indexOf('OPR/') !== -1;
var isChrome = !isOpera && !isEdge && !!navigator.webkitGetUserMedia;

var MediaStream = window.MediaStream;

if (typeof MediaStream === 'undefined' && typeof webkitMediaStream !== 'undefined') {
  MediaStream = webkitMediaStream;
}

/*global MediaStream:true */
if (typeof MediaStream !== 'undefined') {
  if (!('getVideoTracks' in MediaStream.prototype)) {
    MediaStream.prototype.getVideoTracks = function() {
      if (!this.getTracks) {
        return [];
      }

      var tracks = [];
      this.getTracks.forEach(function(track) {
        if (track.kind.toString().indexOf('video') !== -1) {
          tracks.push(track);
        }
      });
      return tracks;
    };

    MediaStream.prototype.getAudioTracks = function() {
      if (!this.getTracks) {
        return [];
      }

      var tracks = [];
      this.getTracks.forEach(function(track) {
        if (track.kind.toString().indexOf('audio') !== -1) {
          tracks.push(track);
        }
      });
      return tracks;
    };
  }

  if (!('stop' in MediaStream.prototype)) {
    MediaStream.prototype.stop = function() {
      this.getAudioTracks().forEach(function(track) {
        if (!!track.stop) {
          track.stop();
        }
      });

      this.getVideoTracks().forEach(function(track) {
        if (!!track.stop) {
          track.stop();
        }
      });
    };
  }
}

function isMediaRecorderCompatible() {
  var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
  var isChrome = !!window.chrome && !isOpera;
  var isFirefox = typeof window.InstallTrigger !== 'undefined';

  if (isFirefox) {
    return true;
  }

  var nVer = navigator.appVersion;
  var nAgt = navigator.userAgent;
  var fullVersion = '' + parseFloat(navigator.appVersion);
  var majorVersion = parseInt(navigator.appVersion, 10);
  var nameOffset, verOffset, ix;

  if (isChrome || isOpera) {
    verOffset = nAgt.indexOf('Chrome');
    fullVersion = nAgt.substring(verOffset + 7);
  }

  // trim the fullVersion string at semicolon/space if present
  if ((ix = fullVersion.indexOf(';')) !== -1) {
    fullVersion = fullVersion.substring(0, ix);
  }

  if ((ix = fullVersion.indexOf(' ')) !== -1) {
    fullVersion = fullVersion.substring(0, ix);
  }

  majorVersion = parseInt('' + fullVersion, 10);

  if (isNaN(majorVersion)) {
    fullVersion = '' + parseFloat(navigator.appVersion);
    majorVersion = parseInt(navigator.appVersion, 10);
  }

  return majorVersion >= 49;
}

function MediaStreamRecorder(streamName, mediaStream, config, streamSender) {
  var mediaRecorder;

  this.record = function() {
    mediaRecorder = new MediaRecorder(mediaStream, config);
    mediaRecorder.ignoreMutedMedia = false;
    mediaRecorder.ondataavailable = function(e) {
      if (!e.data || !e.data.size || e.data.size <= 0) {
        return;
      }
      streamSender(streamName, e.data); // send captured data to server
    };

    mediaRecorder.onerror = function(error) {
      if (error.name === 'InvalidState') {
        console.error('The MediaRecorder is not in a state in which the proposed operation is allowed to be executed.');
      } else if (error.name === 'OutOfMemory') {
        console.error('The UA has exhaused the available memory. User agents SHOULD provide as much additional information as possible in the message attribute.');
      } else if (error.name === 'IllegalStreamModification') {
        console.error('A modification to the stream has occurred that makes it impossible to continue recording. An example would be the addition of a Track while recording is occurring. User agents SHOULD provide as much additional information as possible in the message attribute.');
      } else if (error.name === 'OtherRecordingError') {
        console.error('Used for an fatal error other than those listed above. User agents SHOULD provide as much additional information as possible in the message attribute.');
      } else if (error.name === 'GenericError') {
        console.error('The UA cannot provide the codec or recording option that has been requested.', error);
      } else {
        console.error('MediaRecorder Error', error);
      }

      if (mediaRecorder.state !== 'inactive' && mediaRecorder.state !== 'stopped') {
        mediaRecorder.stop();
      }
    };

    this.getState = function() {
      if (mediaRecorder) {
        return mediaRecorder.state;
      }
      return undefined;
    };

    this.doWhenPossible(function() {
      mediaRecorder.start(1000);
    });
  };

  this.doWhenPossible = function(callback) {
    var inst = this;
    var activator = setInterval(function () {
      try {
        callback();
        clearInterval(activator);
      } catch (ex) {
        console.error(ex);
        if (mediaRecorder && mediaRecorder.state == 'recording') {
          clearInterval(activator);
        }
        console.info("retry starting");
      }
    }, 200);
  };


  /**
   * This method stops recording MediaStream.
   */
  this.stop = function() {
    if (!mediaRecorder) {
      return;
    }

    // mediaRecorder.state === 'recording' means that media recorder is associated with "session"
    // mediaRecorder.state === 'stopped' means that media recorder is detached from the "session" ... in this case; "session" will also be deleted.

    if (mediaRecorder.state === 'recording') {
      mediaRecorder.stop();
    }
  };

  function isMediaStreamActive() {
    if ('active' in mediaStream) {
      if (!mediaStream.active) {
        return false;
      }
    } else if ('ended' in mediaStream) { // old hack
      if (mediaStream.ended) {
        return false;
      }
    }
    return true;
  }

  var self = this;

  // this method checks if media stream is stopped or any track is ended.
  (function looper() {
    if (!mediaRecorder) {
      return;
    }

    if (isMediaStreamActive() === false) {
      if (!config.disableLogs) {
        console.log('MediaStream seems stopped.');
      }
      self.stop();
      return;
    }

    setTimeout(looper, 1000); // check every second
  })();
}

if (typeof RecordHandler !== 'undefined') {
  RecordHandler.MediaStreamRecorder = MediaStreamRecorder;
}