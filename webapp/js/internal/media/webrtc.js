// 'use strict';

var offerOptions = {
  offerToReceiveAudio: 1,
  offerToReceiveVideo: 1
};

function Message(signal, to, conversationUuid, content) {
	this.signal = signal;
	this.to = to;
	this.conversationUuid = conversationUuid;
	this.content = content;
}

function WebRTC(config) {
	if (WebRTC.instance == null) {
		WebRTC.instance = this;
	} else {
		return WebRTC.instance;
	}

  this.conversationUuid = config.conversationUuid;
	this.mediaConfig = config.mediaConfig !== undefined ? config.mediaConfig : null;
	this.type = config.type;

  this.signaling = io.connect(config.host, {port: config.port, resource: 'signalling/binding'});

  this.signaling.on('connect',function() {
    WebRTC.instance.call('connected', "connected to media-server");
  });

  this.signaling.on('message', function(event) {
    var message = JSON.parse(event);
    WebRTC.instance.call(message.signal, message);
  });

  this.signaling.on('close',function(event) {
    WebRTC.instance.call('close', event);
  });

  this.signaling.on('error',function(event) {
    WebRTC.instance.call('error', event);
  });

	this.peerConnections = {};
	this.localStream = null;
	this.sharedStream = null;
	this.signals = {};
	this.nextIsShared = false;

	this.on = function(signal, operation) {
		this.signals[signal] = operation;
	};

	this.call = function(event, data) {
		for (var signal in this.signals) {
			if (event === signal) {
				return this.signals[event](this, data);
			}
		}
		console.log('Event ' + event + ' do not have defined function');
	};

  this.join = function() {
    this.nextIsShared = false;
  	var webrtc = this;
    var handleSuccess = function(stream) {
      webrtc.localStream = stream;
      webrtc.call('localStream', {stream : stream});
      webrtc.send('join');
    };
    var error = function(message) {
      console.info(message);
      webrtc.send('join');
    };
    navigator.mediaDevices.getUserMedia(webrtc.mediaConfig).then(handleSuccess).catch(error);
	};

	this.leave = function() {
	  if (this.localStream) {
      this.localStream.stop();
    }
    if (this.sharedStream) {
      this.sharedStream.stop();
      this.sharedStream = null;
    }
    this.nextIsShared = false;
	this.send('left', null);
    this.closePeerConnections();
	};

  this.sendChatMessage = function(message) {
    this.send('chatMessage', null, message);
  };

	this.send = function(signal, to, content) {
		var json = JSON.stringify(new Message(signal, to, this.conversationUuid, content));
		this.signaling.send(json);
	};

	this.preparePeerConnection = function(webrtc, member) {
		if (webrtc.peerConnections[member] == undefined) {
			var pc = new RTCPeerConnection(config.peerConfig);
			pc.onaddstream = function(event) {
        webrtc.call(webrtc.nextIsShared ? 'sharedStream' : 'remoteStream', {member : member, stream : event.stream});
        webrtc.nextIsShared = false;
			};
			pc.onicecandidate = function(event) {
        function handle(pc, evt){
          if((pc.signalingState || pc.readyState) == 'stable' && webrtc.peerConnections[member]['rem'] == true){
            handleCandidate(webrtc, evt.candidate, member, pc);
            return;
          }
          setTimeout(function(){ handle(pc, evt); }, 2000);
        }
				handle(pc, event);
			};
			webrtc.peerConnections[member] = {};
			webrtc.peerConnections[member]['pc'] = pc;
			webrtc.peerConnections[member]['rem'] = false;
		}
		return webrtc.peerConnections[member];
	};

  this.getPeerConnection = function(webrtc, message) {
    var pc = webrtc.preparePeerConnection(webrtc, message.from);
    if (message.renegotiation && webrtc.sharedStream != null) {
      webrtc.nextIsShared = true;
      pc['pc'].addStream(webrtc.sharedStream);
    }
    else {
      pc['pc'].addStream(webrtc.localStream);
    }
    return pc;
  };

  this.closePeerConnections = function() {
    for (var peer in this.peerConnections) {
      if (this.peerConnections[peer] != undefined) {
        this.peerConnections[peer]['pc'].close();
        this.peerConnections[peer] = undefined;
      }
    }
  };

	this.offer = function(webrtc, message) {
    var pc = webrtc.getPeerConnection(webrtc, message);
    pc['pc'].createOffer(offerOptions).then(function(desc) {
      pc['pc'].setLocalDescription(desc);
      webrtc.send('offerResponse', message.from, desc.sdp);
    })
    .catch(error);
	};

	this.answer = function(webrtc, message) {
    var pc = webrtc.getPeerConnection(webrtc, message);
    var peerConnection = pc['pc'];
    peerConnection.setRemoteDescription(new RTCSessionDescription({
      type : 'offer',
      sdp : message.content
    }), function() {
      pc['rem'] = true;
      pc['pc'].createAnswer(function(desc) {
        pc['pc'].setLocalDescription(desc, function() {
          webrtc.send('answerResponse', message.from, desc.sdp);
        }, error, success);
      }, success);
    }, error);
	};

	this.approve = function(webrtc, message) {
		var pc = webrtc.preparePeerConnection(webrtc, message.from);
		pc['pc'].setRemoteDescription(new RTCSessionDescription({
			type : 'answer',
			sdp : message.content
		}), function(){
			pc['rem'] = true;
		}, error);
	};

	this.disconnect = function(webrtc, event) {
		// this.signaling.disconnect();
	};

	this.candidate = function(webrtc, message) {
		var pc = webrtc.preparePeerConnection(webrtc, message.from);
		pc['pc'].addIceCandidate(new RTCIceCandidate(JSON.parse(message.content.replace(new RegExp('\'', 'g'), '"'))), success, error);
	};

  this.chatMessage = function(webrtc, message) {
    var isMe = message.from == message.to;
    var author = document.createElement("div");
    author.setAttribute("class", "author-name");
    $(author).html(isMe ? "Me" : "Counterparty");

    var text = document.createElement("div");
    text.setAttribute("class", isMe ? "chat-message" : "chat-message active");
    $(text).html(message.content);

    var messageElement = document.createElement("div");
    messageElement.setAttribute("class", isMe ? "left" : "right");
    $(messageElement).append(author);
    $(messageElement).append(text);

    var chatContent = $('.onboarding-chat-content');
    chatContent.append(messageElement);
    chatContent.scrollTop(chatContent[0].scrollHeight);
  };

  this.startScreenSharing = function () {
    var webrtc = this;
    getScreenId(function (error, sourceId, screenConfig) {
      navigator.getUserMedia(screenConfig, function (stream) {
        webrtc.sharedStream = stream;
        webrtc.send('negotiate');
        webrtc.call('sharedStream', {member : "me", stream : webrtc.sharedStream});
      },
      function (error) {
        if (isChrome && location.protocol === 'http:') {
          alert('You\'re not testing it on SSL origin (HTTPS domain) otherwise you didn\'t enable --allow-http-screen-capture command-line flag on canary.');
        } else if (isChrome) {
          alert('Screen capturing is either denied or not supported. Please install chrome extension for screen capturing or run chrome with command-line flag: --enable-usermedia-screen-capturing');
        }

        console.error(error);
      });
    });
  };

	this.init = function() {
		this.on('offer', this.offer);
		this.on('answer', this.answer);
		this.on('approve', this.approve);
		this.on('candidate', this.candidate);
    this.on('chatMessage', this.chatMessage);
		this.on('ping', function(){});
	};

	function handleCandidate(webrtc, candidate, member, destPC) {
		if (candidate) {
		  //todo: check destPC
			webrtc.send('candidate', member, JSON.stringify(candidate));
		}
	}

	this.init();
}

WebRTC.instance = null;

WebRTC.onReady = function() {
	console.log('It is highly recommended to override method WebRTC.onReady');
};

var error = function(error) {
	console.info(error);
};

var success = function(success) {
};
