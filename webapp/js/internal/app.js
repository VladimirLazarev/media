var mainModule;
  (function () {
    mainModule = angular.module('MainModule', [
        'ui.router',                    // Routing
        'ui.bootstrap'                  // Ui Bootstrap
    ], function($httpProvider) {
      // Use x-www-form-urlencoded Content-Type
      $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

      // Override $http service's default transformRequest
      $httpProvider.defaults.transformRequest = [function(data) {
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
      }];
    })
    .config(['$httpProvider', function($httpProvider, $contextPath) {
        $httpProvider.interceptors.push(function() {
          return {
            'request': function(config) {
              if (! config.url.indexOf("$contextPath") == 0) {
                config.url = $contextPath + (config.url.indexOf("/") == 0 ? "" : "/") + config.url;
              }
              return config;
            }
          };
        });
      }])
})();

function config($stateProvider, $locationProvider) {

    $locationProvider.html5Mode(
        {
            enabled: false,
            requireBase: true,
            rewriteLinks: true
        }
    ).hashPrefix('!');

    $stateProvider
        .state('system', {
            templateUrl: "/signalling/video"
        });
}

function MainCtrl($rootScope, $scope) {
    $scope.events = [];
    function addEvent(evt) {
        $scope.$evalAsync(function() {
            $scope.events.push(evt);
        })
    }
}

mainModule
    .config(config)
    .run(function ($rootScope, $state) {
        $rootScope.$state = $state;
    });

mainModule
    .controller('mainCtrl', MainCtrl)
    .controller('meetingCtrl', MeetingCtrl);

angular.module('MainModule');