# WebRTC java media server #

The [WebRTC technology](https://webrtc.org) is using [Session Description Protocol](https://tools.ietf.org/html/rfc4566) for handling of voice-over-ip calls and streaming video. It requires implementation and deployment of media server for exchanging of metadata required for handling of WebRTC sessions. The proper design of WebRTC based solution looks like the picture below:

![webrtc_design.png](https://bitbucket.org/repo/nqaknE/images/2711053627-webrtc_design.png)

The are two peers, located behind the firewalls, who want to connect to each other using WebRTC technology, and video solution provider, who can belong to one of the peers or be independent vendor. The topology of networks usually complicates peers discovery and makes direct connection impossible. To discover each other they need to use a media server, deployed on video solution provider’s infrastructure, which will be responsible for initiating and managing of session. The establishing of video session through the media server can be described as the following sequence diagram:

![signaling.png](https://bitbucket.org/repo/nqaknE/images/1051130983-signaling.png)


### Important facts about implementation ###

That media server is written on java language and can be deployed on any application server which support java applications.
The media server has embedded jetty application server to simplify development process. It can be also used for production version, or evicted and replaced by application server of your choice. 

This implementation is built around [Spring framework](https://projects.spring.io/spring-framework/) to utilize its dependency injection pattern for components based approach.

The [Atmosphere framework](https://github.com/Atmosphere/atmosphere) used in that implementation adds support for both websocket and http protocols via socket.io engine, which allows to switch communication protocols dynamically. If client has secured websocket enabled, then Socket.IO uses WSS, if not, then long polling on HTTPS will be used. This solution is more efficient and stable for various network configurations, as not all organization proxies allow usage of WSS.

### Scripts for web-application Front-End ###

The implementation of media server is not intended to provide any Front-End scripts, however it might be useful to demonstrate how that media server can be integrated with web-application. Those scripts were added as part of simple embedded web-application: see webapp folder. The js sub-folder contains two categories of scripts:

* external - for importing of some well knows scripts: angular, bootstrap, jquery
* internal - scripts needed for integration of your web-application with WebRTC based solution. 

Some details about internal category script files:

| Script file       | Description                    |
| ----------------- | ---------------------------------------------------------------------------------------------------------------------------------- |
| `meeting.js`      | Angular JS controller for handling of video meeting page flow. It contains binded actions (leave/join) to control video meeting    |
|                   | and actions to react on events received from media server                                                                          |
| `webrtc.js`       | JavaScript file for integration with media-server. This script initiates connection to media-server and handles requests/responses |                                                                    | `socket.io.js`    | JavaScript library for web-applications to simplify data exchange in real time. Provides dynamic switching of connectivity protocol| 
| `record.js`       | The javascript module for intercepting and recording of video streams. It sends every second chunks of data to web-application     |
|                   | back-end server.                                                                                                                   |
| `ice_validator.js`| Script for TURN server connectivity checks                                                                                         |
| `adapter.js`      | [WebRTC adapter](https://github.com/webrtc/adapter) to wrap low level WebRTC invocations                                           |



### License ###

This implementation is published under [MIT license](https://en.wikipedia.org/wiki/MIT_License).